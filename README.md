dotfiles
==========================

###### It's a fork of [repo](https://github.com/alrra/dotfiles)


These are the base dotfiles that I start with when I set up a new
environment. For more specific local needs I use the `*.local` files
described in the [Local Settings](#local-settings) section.


Setup
-----

To set up the dotfiles run the appropriate snippet in the terminal:

(⚠️  **DO NOT** run the `setup` snippet if you do not fully understand
[what it does][setup]. Seriously, **DON'T**!)

| Snippet |
|:---|
| `bash -c "$(curl -LsS https://gitlab.com/vnepogodin/dotfiles/-/raw/develop/src/os/setup.sh)"` |
| or |
| `bash -c "$(wget -qO - https://gitlab.com/vnepogodin/dotfiles/-/raw/develop/src/os/setup.sh)"` |

That's it! ✨

The setup process will:

* Download the dotfiles on your computer
  (by default it will suggest `~/projects/dotfiles`).
* Create some additional [directories][dirs].
* Copy the [Git][git], [shell][shell],
  and [Vim][vim] files.
* Install applications / command-line tools for
  [Arch][install arch].
* Set custom [Arch][preferences arch] preferences.
* Install [Vim plugins][vim plugins].

Customize
---------

### Local Settings

The dotfiles can be easily extended to suit additional local
requirements by using the following files:

#### `~/.bash.local`

The `~/.bash.local` file will be automatically sourced after all
the other [Bash related files][shell], thus, allowing its content
to add to or overwrite the existing aliases, settings, `PATH`, etc.

Here is an example:


```shell
#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Set PATH additions.

PATH="/Users/vnepogodin/projects/dotfiles/src/bin/:$PATH"

export PATH

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# Set local aliases.

alias g="git"
```

#### `~/.gitconfig.local`

The `~/.gitconfig.local` file will be automatically included after
the configurations from `~/.gitconfig`, thus, allowing its content
to overwrite or add to the existing Git configurations.

__Note:__ Use `~/.gitconfig.local` to store sensitive information
such as the Git user credentials, e.g.:

```gitconfig
[commit]

    # Sign commits using GPG.
    # https://help.github.com/articles/signing-commits-using-gpg/

    gpgSign = true

[user]

    name = Vladislav Nepogodin
    email = account@example.com
    signingKey = XXXXXXXX
```

### Forks

If you decide to [fork] this project, do not forget to substitute
my username with your own in the [`setup` snippets](#setup) and
[in the `setup` script][setup script].

Update
------

To update the dotfiles you can either run the [`setup` script][setup]
or, if you want to update one particular part, run the appropriate
[`os` script](src/os).

License
-------

The code is available under the [MIT license][license].

<!-- Link labels: -->


[dirs]: src/os/create_directories.sh
[fork]: https://help.github.com/en/github/getting-started-with-github/fork-a-repo
[git]: src/git
[install arch]: src/os/install/arch
[license]: LICENSE
[preferences arch]: src/os/preferences/arch
[repo]: https://github.com/alrra
[setup script]: https://gitlab.com/vnepogodin/dotfiles/-/blob/develop/src/os/setup.sh#L3
[setup]: src/os/setup.sh
[shell]: src/shell
[vim plugins]: https://gitlab.com/vnepogodin/dotfiles/-/blob/develop/src/vim/vimrc#L56
[vim]: src/vim
