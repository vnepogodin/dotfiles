#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "./utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n   Terminal\n\n"

install_package "Alacritty" "alacritty"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

install_package "Oh My Zsh" "zsh oh-my-zsh-git fzf"
