#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "./utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n   Miscellaneous Tools\n\n"

install_package "cURL" "curl"
install_package "cli JSON processor" "jq"
install_package "bemenu" "bemenu"
install_package "Slurp" "slurp"
install_package "grim" "grim"
install_package "Wayland clipboard" "wl-clipboard"
