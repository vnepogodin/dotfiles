#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


./aur.sh

update
upgrade

./git.sh
./build-essentials.sh
./shell.sh
./browsers.sh
./compression_tools.sh
./terminal.sh
./misc.sh
./misc_tools.sh
./../vim.sh

./cleanup.sh
