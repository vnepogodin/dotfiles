#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "./utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n   Build Essentials\n\n"

# Install tools for compiling/building software from source.

install_package "Gnu Compiler Collection" "gcc"
install_package "Gnu Debugger" "gdb"
install_package "Cmake" "cmake"
install_package "GnuPG archive keys" "gnome-keyring"
