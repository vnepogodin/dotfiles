#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

autoremove() {

    # Remove packages that were automatically installed to satisfy
    # dependencies for other packages and are no longer needed.

    execute \
        "paru --clean --noconfirm &> /dev/null" \
        "paru (autoremove)"

}

install_package() {

    declare -r EXTRA_ARGUMENTS="$3"
    declare -r PACKAGE="$2"
    declare -r PACKAGE_READABLE_NAME="$1"

    if ! package_is_installed "$PACKAGE"; then
        execute "paru -S --noconfirm $EXTRA_ARGUMENTS $PACKAGE &> /dev/null" "$PACKAGE_READABLE_NAME"
    else
        print_success "$PACKAGE_READABLE_NAME"
    fi

}

package_is_installed() {
    paru -Ql "$1" &> /dev/null
}

update() {

    # Resynchronize the package index files from their sources.

    execute \
        "paru -Sy &> /dev/null" \
        "pacman (update)"

}

upgrade() {

    # Install the newest versions of all packages installed.

    execute \
        "paru -Su --noconfirm &> /dev/null" \
        "pacman (upgrade)"

}
