#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "./utils.sh"


# Install shell and terminal emulator
execute \
    "paru -S --noconfirm zsh alacritty >/dev/null" \
    "Terminal emulator"

# Install shell deps
execute \
    "paru -S --noconfirm oh-my-zsh-git zsh-theme-powerlevel10k >/dev/null" \
    "Shell deps"
