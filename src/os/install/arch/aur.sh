#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "./utils.sh"


# Install AUR helper
execute \
    "sudo pacman -S --noconfirm git >/dev/null && \
     git clone --quiet https://aur.archlinux.org/paru-bin.git && \
     cd paru-bin && makepkg -sci --noconfirm >/dev/null" \
    "AUR helper (install)"

execute \
    "paru -S --noconfirm chaotic-keyring chaotic-mirrorlist >/dev/null &&
     sudo tee -a /etc/pacman.conf >/dev/null <<'EOF'

[chaotic-aur]
Include = /etc/pacman.d/chaotic-mirrorlist

EOF" \
    "Mirrors (install)"
