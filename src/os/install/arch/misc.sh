#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh" \
    && . "./utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n   Miscellaneous\n\n"

install_package "mpv" "mpv"
install_package "ImV" "imv"
install_package "Open" "linopen"
install_package "rofi launcher" "rofi-lbonn-wayland-git"
install_package "wofi launcher" "wofi"
install_package "Lightweight notification daemon for Wayland" "mako"
install_package "Wayland color picker" "wl-color-picker"
