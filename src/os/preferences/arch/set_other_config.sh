#!/bin/bash

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

if [ ! -d ~/.config/{chill_app,dunst,i3,polybar,waybar,rofi,wofi,contour,nvim,xob} ]; then
    cp -rf {./,~/.}config
else
    cp -f {./,~/.}config/{chill_app,dunst,i3,polybar,waybar,rofi,wofi,contour,nvim,xob}{,/*}
    cp -f {./,~/.}config/wayfire.ini
fi
