#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n   UI & UX\n\n"


function make_copy() {

    declare -a FILES=(

    "./scripts/arch_updates.sh"
    "./scripts/dman"
    "./scripts/dmconf"
    "./scripts/dmenusearch.sh"
    "./scripts/dmkill"
    "./scripts/dmshot"
    "./scripts/exit_menu"
    "./scripts/linux_ver.sh"
    "./scripts/print_ascii.py"
    "./scripts/title"
    "./scripts/updates.sh"
    "./scripts/weather"
    "./scripts/wofipowermenu.sh"

    "./config/sway/config"
    "./config/yambar/config.yml"
    "./config/yambar/launch.sh"

    )

    create_config "$(declare -p FILES)"

}

execute make_copy "Set configs"

#execute
#"if [ ! -f ~/.scripts ]; then
#    cp -rf {./,~/}.scripts
#else
#    cp -rf ./.scripts/* ~/.scripts
#fi" \
#    "Prepare for the Sway and bar"

#execute
#"if [ ! -f ~/.config/sway ]; then
#    cp -rf {./,~/}.config/sway
#else
#    cp -rf ./.config/sway/* ~/.config/sway
#fi" \
#    "Set Sway config"

#execute
#"if [ ! -f ~/.config/yambar ]; then
#    cp -rf {./,~/}.config/yambar
#else
#    cp -rf ./.config/yambar/* ~/.config/yambar
#fi" \
#    "Set yambar config"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

execute "sudo cp -f {./,/}etc/linopen.conf"
    "Set open config"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

execute
"if [ ! -f ~/Pictures ]; then
    cp -rf ./Pictures   ~/Pictures
else
    cp -rf ./Pictures/* ~/Pictures
fi" \
    "Set backgrounds"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

execute "./set_other_config.sh" "Set other configs"
