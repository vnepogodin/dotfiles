#!/bin/bash

search_term="$(bemenu -i -p 'DuckDuckGo search:' | tr ' ' '+')";

test -n "$search_term" && open "https://duckduckgo.com/?t=ffab&q=$search_term";
