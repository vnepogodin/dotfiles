main() {
    while 1; do
        [ -z $WAYLAND ]\
            && xsetroot -name "$(get_status)"\
            && arg='&'\
            || echo "$(get_status)";

        sleep 5;
    done;
}

main ${arg};
