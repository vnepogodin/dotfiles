#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "../../utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

print_in_purple "\n   Terminal\n\n"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

execute
"if [ ! -d ~/.config/alacritty ]; then
    cp -rf ./.config/alacritty ~/.config/alacritty
else
    cp -rf ./.config/alacritty/* ~/.config/alacritty
fi" \
    "Set custom terminal config"
