vim.g.coc_global_extensions = {'coc-clangd'}
vim.g.coc_default_semantic_highlight_groups = 1

vim.cmd [[
augroup my_coc
    autocmd!

    let b:coc_diagnostic_disable = 1

    call coc#config('coc.source', {
    \ 'around': {'enable': 0},
    \ 'buffer': {'enable': 1},
    \})
    call coc#config('clangd.semanticHighlighting', 1)

    autocmd User CocJumpPlaceholder silent call CocActionAsync('showSignatureHelp')

    function! ShowDocumentation()
        if CocAction('hasProvider', 'hover')
            call CocActionAsync('doHover')
        else
            call feedkeys('K', 'in')
        endif
    endfunction

    " Highlight the symbol and its references when holding the cursor.
    autocmd CursorHold * silent call CocActionAsync('highlight')

augroup end
]]
