#!/usr/bin/env sh

# Terminate already running bar instances
killall -q yambar

# Wait until the processes have been shut down
while pgrep -u $UID -x yambar >/dev/null; do sleep 1; done

yambar -c ~/.config/yambar/config.yml & disown
