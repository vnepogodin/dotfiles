#!/bin/sh

# Terminate already running instances
killall -q chill_server gammastep

chill_server & disown
gammastep & disown
