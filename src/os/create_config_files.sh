#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")" \
    && . "utils.sh"

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

copy_config() {

    declare -a FILES=(

        "shell/bash_functions"
        "shell/bashrc"
        "shell/zshrc"
        "shell/curlrc"

        "git/gitconfig"

        "vim/vimrc"

    )

    create_config "$(declare -p FILES)"
}

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

main() {
    print_in_purple "\n • Create config files\n\n"
    copy_config "$@"
}

main "$@"
